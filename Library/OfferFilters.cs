﻿namespace Library {
    public class OfferFilters
    {
        public int minPrice { get; set; }
        public int maxPrice { get; set; }
        public string processorSeries { get; set; } = "";
        public string diskType { get; set; } = "";
        public int minDiskSize { get; set; }
        public int minRamSize { get; set; }
        public string matrixResolution { get; set; } = "";
    }
}
