﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library
{
    public class UserStatistic
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string OfferUrl { get; set; }
        public string Browser { get; set; }
        public DateTime VisitDate { get; set; }
    }
}
