﻿using System;

namespace Library{
    public class Offer{
        public int Id { get; set; }
        public int authorId { get; set; }
        public int sourceId { get; set; }
        public int opinionId { get; set; }
        public string Url { get; set; }
        public string imageUrl { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public float diskSize { get; set; }
        public string diskType { get; set; }
        public string processorSeries { get; set; }
        public string processorModel { get; set; }
        public string matrixSize { get; set; }
        public string matrixResolution { get; set; }
        public string matrixType { get; set; }
        public string matrixCoating { get; set; }
        public float ramSize { get; set; }
        public string ramType { get; set; }
        public string graphicsCardType { get; set; }
        public string graphicsCardModel { get; set; }
        public float graphicsCardMemory { get; set; }
        public string operatingSystem { get; set; }
        public decimal weight { get; set; }
    }

    public class OfferSource {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class OfferAuthor {
        public int Id { get; set; }
        public string Name { get; set; }
        public string externalId { get; set; }
        public decimal rating { get; set; }
        public int recommendCount { get; set; }
        public int notRecommendCount { get; set; }
    }

    public class ProductOpinion {
        public int id { get; set; }
        public string modelName { get; set; }
        public string opinionSource { get; set; }
        public decimal rating { get; set; }
        public int numberOfReviews { get; set; }
    }


    public class GraphicsCard {
        public int id { get; set; }
        public int rank { get; set; }
        public string graphicsCardName { get; set; }
    }


    public class Processor {
        public int id { get; set; }
        public int rank { get; set; }
        public string processorName { get; set; }
    }

}
