﻿namespace Library {
    public class NotebookCharacteristics {
        public int processorRate { get; set; }
        public int graphicsCardRate { get; set; }
        public int diskRate { get; set; }
        public int matrixRate { get; set; }
        public int priceRate { get; set; }
    }
}
