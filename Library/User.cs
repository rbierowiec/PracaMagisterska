﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library
{
    public class User
    {
        public int Id { get; set; }
        public string Profile { get; set; }
        public string Hash { get; set; }
        public string Ip { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
