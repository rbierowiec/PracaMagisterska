﻿using HtmlAgilityPack;
using Library;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net;

namespace OffersFromHTMLParser {
    public class OfferAuthorDetailsDownloader{
        public OfferAuthor getAuthorDetails(HtmlDocument doc) {
            string authorRatingsHref = doc.DocumentNode.SelectNodes("//div[contains(@class, 'btn-user')]").First().SelectSingleNode(".//a[contains(@class, 'alleLink')]").Attributes["href"].Value;
            string authorName = doc.DocumentNode.SelectNodes("//div[contains(@class, 'btn-user')]").First().SelectSingleNode(".//a[contains(@class, 'alleLink')]").InnerText.Trim();
            string authorId = authorRatingsHref.Split("/")[2];

            string jsonRatings = WebUtility.HtmlDecode(doc.DocumentNode.SelectNodes("//div[contains(@class, 'seller-details')]").First().Attributes["data-userrating"].Value);

            decimal ratings = 0;
            int numberOfRatingsType = 0;

            int recommendCount = 0;
            int notRecommendCount = 0;

            if (jsonRatings.Length > 0) {
                JObject userObject = JsonConvert.DeserializeObject<JObject>(WebUtility.HtmlDecode(doc.DocumentNode.SelectNodes("//div[contains(@class, 'seller-details')]").First().Attributes["data-userrating"].Value));
                JObject userRatings = userObject["averageRates"].Value<JObject>();

                recommendCount = int.Parse(userObject["recommendCount"].Value<string>());
                notRecommendCount = int.Parse(userObject["notRecommendCount"].Value<string>());

                foreach (var property in userRatings.Properties()) {
                    ratings += decimal.Parse(((string)property.Value).Replace(".", ","));
                    ++numberOfRatingsType;
                }
            }

            decimal finalRating = 0;
            if (numberOfRatingsType > 0) {
                finalRating = ratings / numberOfRatingsType;
            }


            OfferAuthor author = new OfferAuthor { Name = authorName, externalId = authorId, rating = finalRating, recommendCount = recommendCount, notRecommendCount = notRecommendCount };
            return author;
        }
    }
}
