﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Collections.Generic;

namespace OffersManager {
    public class HtmlContentManager
    {
        public List<string> downloadUrlsFromPage(int pageNumber = 1) {
            string urlAddress = "https://allegro.pl/kategoria/laptopy-491?order=n&offerTypeBuyNow=1&stan=nowe&p=" + pageNumber;
            List<string> offers = new List<string>();

            OfferDetailsDownloader detailsManager = new OfferDetailsDownloader();

            WebClient webClient = new WebClient();
            webClient.Headers.Add("user-agent", "	Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0");

            string page = webClient.DownloadString(urlAddress);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page);

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//article"))
            {
                try
                {
                    string nodeClass = node.Attributes["class"].Value;

                    // Pominięcie ofert sponsorowanych oraz artykułów pod ofertami
                    if(nodeClass.IndexOf("carousel-item") == -1 && node.SelectSingleNode(".//a").Attributes["href"].Value.IndexOf("clicks?") == -1)
                    {
                        offers.Add(node.SelectSingleNode(".//a").Attributes["href"].Value);
                    }
                }
                catch (NullReferenceException){
                    Console.WriteLine("Null value");
                }
            }

            return offers;
        }

        public List<String> downloadAllUrls() {
            List<String> offers = new List<String>();
            string urlAddress = "https://allegro.pl/kategoria/laptopy-491?order=n&offerTypeBuyNow=1";

            WebClient webClient = new WebClient();
            webClient.Headers.Add("user-agent", "	Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0");

            string page = webClient.DownloadString(urlAddress);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page);

            int maxPageNumber = 2;// int.Parse(doc.DocumentNode.SelectNodes("//a[contains(@rel, 'last')]").First().InnerHtml);

            for (int i = 1; i <= maxPageNumber; i++) {
                offers.AddRange(this.downloadUrlsFromPage(i));
            }

            return offers;
        }
    }
}
