﻿using HtmlAgilityPack;
using Library;
using OffersFromHTMLParser;
using System;
using System.Linq;
using System.Net;

namespace OffersManager {
    public class OfferDetailsDownloader {
        private OfferAuthorDetailsDownloader authorDetailsDownloader = new OfferAuthorDetailsDownloader();

        public (Offer, OfferAuthor) downloadOfferDetails(String Url) {
            Offer offer = new Offer { Url = Url };

            WebClient webClient = new WebClient();
            webClient.Headers.Add("user-agent", "	Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0");

            string page = webClient.DownloadString(Url);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page);

            string title = doc.DocumentNode.SelectNodes("//h1[contains(@class, 'title')]").First().InnerHtml;
            offer.Name = title.Substring(0, title.IndexOf("<small>")).Trim();

            string price = doc.DocumentNode.SelectNodes("//div[contains(@class, 'price')]").First().InnerText.Replace(" zł", "");
            offer.Price = Decimal.Parse(price);

            string imageUrl = doc.DocumentNode.SelectNodes("//img[contains(@class, 'img-responsive')]").First().Attributes["src"].Value;
            offer.imageUrl = imageUrl;

            try {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//ul").Where(x => x.Attributes["class"].Value.Equals("_72a127f7"))) {
                    foreach (var attribute in node.SelectNodes(".//li")) {
                        var attributeType = attribute.SelectNodes(".//div").Where(x => x.Attributes["class"].Value.IndexOf("a853916b") >= 0).First().InnerText;
                        var attributeValue = attribute.SelectNodes(".//div").Where(x => x.Attributes["class"].Value.IndexOf("_09810109") >= 0).First().InnerText;
                        Console.WriteLine(attributeType + " " + attributeValue);
                        if (attributeType.IndexOf("Wielkość matrycy") >= 0) {
                            offer.matrixSize = attributeValue;
                        } else if (attributeType.IndexOf("Seria procesora") >= 0) {
                            offer.processorSeries = attributeValue;
                        } else if (attributeType.IndexOf("Model procesora") >= 0) {
                            offer.processorModel = attributeValue;
                        } else if (attributeType.IndexOf("Pojemność dysku") >= 0) {
                            offer.diskSize = float.Parse(attributeValue);
                        } else if (attributeType.IndexOf("Typ dysku twardego") >= 0) {
                            offer.diskType = attributeValue;
                        } else if (attributeType.IndexOf("Typ pamięci RAM") >= 0) {
                            offer.ramType = attributeValue;
                        } else if (attributeType.IndexOf("Wielkość pamięci RAM") >= 0 && attributeType.IndexOf("maksymalna") == -1) {
                            if (attributeValue.IndexOf("GB") >= 0) {
                                attributeValue = attributeValue.Replace(" GB", "");
                                if (float.TryParse(attributeValue, out var number)) {
                                    offer.ramSize = number;
                                }
                            } else if (attributeValue.IndexOf("MB") >= 0) {
                                attributeValue = attributeValue.Replace(" MB", "");
                                if (float.TryParse(attributeValue, out var number)) {
                                    offer.ramSize = number / 1000;
                                }
                            }
                        } else if (attributeType.IndexOf("Rodzaj karty graficznej") >= 0) {
                            offer.graphicsCardType = attributeValue;
                        } else if (attributeType.IndexOf("Pamięć karty graficznej") >= 0) {
                            if (attributeValue.IndexOf("GB") >= 0) {
                                attributeValue = attributeValue.Replace(" GB", "");
                                if (float.TryParse(attributeValue, out var number)) {
                                    offer.graphicsCardMemory = number;
                                }
                            } else if (attributeValue.IndexOf("MB") >= 0) {
                                attributeValue = attributeValue.Replace(" MB", "");
                                if (float.TryParse(attributeValue, out var number)) {
                                    offer.graphicsCardMemory = number / 1000;
                                }
                            }
                        } else if (attributeType.IndexOf("Model karty graficznej") >= 0) {
                            offer.graphicsCardModel = attributeValue;
                        } else if (attributeType.IndexOf("Rozdzielczość") >= 0) {
                            offer.matrixResolution = attributeValue;
                        } else if (attributeType.IndexOf("Typ matrycy") >= 0) {
                            offer.matrixType = attributeValue;
                        } else if (attributeType.IndexOf("waga") >= 0) {
                            attributeValue = attributeValue.Replace(" [kg]", "");
                            if (decimal.TryParse(attributeValue, out var number)) {
                                offer.weight = number;
                            }
                        } else if (attributeType.IndexOf("System operacyjny") >= 0) {
                            offer.operatingSystem = attributeValue;
                        } else if (attributeType.IndexOf("Powłoka matrycy") >= 0) {
                            offer.matrixCoating = attributeValue;
                        }
                    }
                }

            } catch (NullReferenceException) {
                Console.WriteLine("Null value");
            }

            OfferAuthor author = this.authorDetailsDownloader.getAuthorDetails(doc);

            return (offer, author);
        }
    }
}
