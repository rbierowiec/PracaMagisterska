﻿using HtmlAgilityPack;
using Library;
using System;
using System.Linq;
using System.Net;
using System.Web;

namespace OffersFromHTMLParser.FeedbackSearchEngine {
    public class FeedbackSearchEngine
    {
        private string findFeedbackUrl(String laptopDescription) {
            // Przygotowanie adresu url za pomocą którego poszukamy produktu na stronie ceneo
            String baseUrl = "https://www.google.pl/search?q=" + HttpUtility.UrlEncode(laptopDescription + " ceneo");

            WebClient webClient = new WebClient();
            Console.WriteLine(baseUrl);
            string page = webClient.DownloadString(baseUrl);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page);

            string ceneoUrl = "";

            try {
                ceneoUrl = doc.DocumentNode.SelectNodes("//a[contains(@href, 'ceneo.pl/')]").First().Attributes["href"].Value;

                // Google zwraca adres w rozbudowanym formacie, musimy go okroić, aby wyciągnąć jedynie interesujący nas link
                ceneoUrl = ceneoUrl.Substring(ceneoUrl.IndexOf("https://"), ceneoUrl.IndexOf("&amp") - ceneoUrl.IndexOf("https://"));
            } catch { }

            return ceneoUrl;
        }

        public ProductOpinion getFeedback(String laptopDescription) {
            ProductOpinion opinion = new ProductOpinion();
            opinion.opinionSource = this.findFeedbackUrl(laptopDescription);

            if(opinion.opinionSource != "") {

                WebClient webClient = new WebClient();
                webClient.Headers.Add("user-agent", "	Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0");

                string page = webClient.DownloadString(opinion.opinionSource);

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(page);

                Console.WriteLine(opinion.opinionSource);
                Console.WriteLine(page);

                try {
                    opinion.modelName = doc.DocumentNode.SelectNodes("//dd/strong").First().InnerText;
                } catch {
                    Console.WriteLine("Error: model name not found");
                }

                try {
                    opinion.rating = Decimal.Parse(doc.DocumentNode.SelectNodes("//span[contains(@class, 'product-score')]").First().Attributes["content"].Value.Replace('.', ','));
                } catch {
                    Console.WriteLine("Error: model rating not found");
                }

                int numberOfReviews = 0;
                try {
                    numberOfReviews += int.Parse(doc.DocumentNode.SelectNodes("//span[contains(@itemprop, 'ratingCount')]").First().InnerText);
                } catch {
                    Console.WriteLine("Error: number of rating not found");
                }

                try {
                    numberOfReviews += int.Parse(doc.DocumentNode.SelectNodes("//span[contains(@itemprop, 'reviewCount')]").First().InnerText);
                } catch {
                    Console.WriteLine("Error: number of review not found");
                }

                opinion.numberOfReviews = numberOfReviews;
            }

            return opinion;
        }
    }
}
