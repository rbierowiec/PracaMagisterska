﻿using HtmlAgilityPack;
using Library;
using System;
using System.Collections.Generic;
using System.Net;

namespace OffersFromHTMLParser.NotebookPartsRankingsDownloader {
    public class graphicsCardsRankingDownloader {
        public List<GraphicsCard> downloadGraphicsCardsRanking() {
            List<GraphicsCard> graphicsCards = new List<GraphicsCard>();
            string urlAddress = "https://www.notebookcheck.pl/Ranking-kart-graficznych-laptopy.1174.0.html";

            WebClient webClient = new WebClient();
            string page = webClient.DownloadString(urlAddress);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page);

            int rank = 1;
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[contains(@id, 'sortierbare_tabelle')]/tr")) {
                try {
                    if (node.Attributes["class"].Value.IndexOf("odd") >= 0 || node.Attributes["class"].Value.IndexOf("even") >= 0) {
                        string graphicsCardName = node.SelectSingleNode("./td[2]").InnerText;
                        GraphicsCard item = new GraphicsCard { rank = rank++, graphicsCardName = graphicsCardName };

                        graphicsCards.Add(item);
                    }
                } catch {
                    Console.WriteLine("No second td");
                }
            }

            return graphicsCards;
        }
    }
}
