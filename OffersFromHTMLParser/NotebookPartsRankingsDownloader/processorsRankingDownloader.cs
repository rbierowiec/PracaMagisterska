﻿using HtmlAgilityPack;
using Library;
using System;
using System.Collections.Generic;
using System.Net;

namespace OffersFromHTMLParser.NotebookPartsRankingsDownloader {
    public class processorsRankingDownloader {
        public List<Processor> downloadProcessorsRanking() {
            List<Processor> graphicsCards = new List<Processor>();
            string urlAddress = "https://www.notebookcheck.pl/Ranking-procesorow-mobilnych-w-laptopach.4344.0.html";

            WebClient webClient = new WebClient();
            string page = webClient.DownloadString(urlAddress);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page);

            int rank = 1;
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[contains(@id, 'sortierbare_tabelle')]/tr")) {
                try {
                    if (node.Attributes["class"].Value.IndexOf("odd") >= 0 || node.Attributes["class"].Value.IndexOf("even") >= 0) {
                        string processorName = node.SelectSingleNode("./td[2]").InnerText;
                        Processor item = new Processor { rank = rank++, processorName = processorName };

                        graphicsCards.Add(item);
                    }
                } catch {
                    Console.WriteLine("No second td");
                }
            }

            return graphicsCards;
        }
    }
}
