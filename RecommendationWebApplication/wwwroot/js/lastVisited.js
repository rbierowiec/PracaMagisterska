﻿class lastVisited {
    prepareOfferCard(offerDetails) {
        var str = "";

        str += '<div class="card offerCard">';
        str += '<div class="card-img-top image">';
            str += '<img src="' + offerDetails['imageUrl'] + '">';
        str += '</div>';
                str += '<div class="card-body">';
                    str += '<h4 class="card-title">' + offerDetails['name'].substr(0, 30) + '</h4>';
                    str += '<p class="card-text">' + this.prepareOfferDescription(offerDetails) + '</p>';
                    str += '<a href="' + offerDetails['url'] + '" class="btn btn-primary showOfferButton">Show offer</a>';
                str += '</div>';
        str += '</div>';

        return str;
    }

    prepareOfferDescription(offerDetails) {
        var str = "";
        
        if (offerDetails['processorModel'] != null) { str += "Processor: " + offerDetails['processorModel'] + "<br/>"; }
        if (offerDetails['graphicsCardModel'] != null) { str += "Graphics Card: " + offerDetails['graphicsCardModel'] + "<br/>"; }
        if (offerDetails['ramSize'] != null) { str += "RAM size: " + offerDetails['ramSize'] + " GB<br/>"; }
        if (offerDetails['diskType'] != null) { str += "Disk type: " + offerDetails['diskType'] + "<br/>"; }
        if (offerDetails['matrixResolution'] != null) { str += "Matrix resolution: " + offerDetails['matrixResolution'] + " <br/><br/>"; }
        if (offerDetails['price'] != null) { str += "Price: " + offerDetails['price'] + " zł"; }

        return str;
    }

    loadOffers() {
        preloader.addPreloader();
        var classInstance = this;
        $.ajax({
            url: offersServiceUrl + "/lastVisited",
            method: "POST",
            data: {userHash: cookie.get('user_hash')},
            success: function (response) {
                $.each(response, function (key, value) {
                    $('.visitedContainer').append(classInstance.prepareOfferCard(value));
                });
                preloader.removePreloader();
            }
        });
    }
}


$(document).ready(function () {
    var lastVisitedInstance = new lastVisited();
    lastVisitedInstance.loadOffers();
});