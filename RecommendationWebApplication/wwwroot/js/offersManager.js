﻿class offersManager {
    constructor() {
        this.offers = [];
    }

    prepareOfferCard(offerDetails) {
        var str = "";

        str += '<div class="card offerCard">';
            str += '<div class="row ">';
                str += '<div class="col-md-4">'
                    str += '<div class="image">';
                        str += '<img src="' + offerDetails['imageUrl'] + '" class="w-100">';
                    str += '</div>';
                str += '</div>';
                str += '<div class="col-md-8 px-3">';
                    str += '<div class="card-block px-3">';
                        str += '<h4 class="card-title">' + offerDetails['Name'] + '</h4>';
                        str += '<p class="card-text">' + this.prepareOfferDescription(offerDetails) + '</p>';
                        str += '<a href="' + offerDetails['Url'] + '" class="btn btn-primary showOfferButton">Show offer</a>';
        
                        if (offerDetails['authorRatings'] != "" && parseFloat(offerDetails['authorRatings']) < 3) {
                            str += '<button class="btn btn-danger ratingButton" title="Offer author got very bad ratings!"><i class="far fa-frown"></i></button>';
                        } else if (offerDetails['authorRatings'] != "" && parseFloat(offerDetails['authorRatings']) < 4) {
                            str += '<button class="btn btn-warning ratingButton" title="Offer author got ratings below average."><i class="far fa-meh"></i></button>';
                        }


                        if (offerDetails['laptopRating'] != "" && parseFloat(offerDetails['laptopRating']) < 3) {
                            str += '<button class="btn btn-danger ratingButton" title="This product got very low ratings!"><i class="far fa-star-half"></i></button>';
                        } else if (offerDetails['laptopRating'] != "" && parseFloat(offerDetails['laptopRating']) < 4) {
                            str += '<button class="btn btn-warning ratingButton" title="This product got ratings below average."><i class="far fa-star-half"></i></button>';
                        }



                    str += '</div>';
                str += '</div>';
            str += '</div>';
        str += '</div>';

        return str;
    }

    prepareOfferDescription(offerDetails) {
        var str = "";

        if (offerDetails['rating'] != "") { str += "Rating: " + offerDetails['rating'] + "<br/><br/>"; }
        if (offerDetails['processorModel'] != "") { str += "Processor: " + offerDetails['processorModel'] + "<br/>"; }
        if (offerDetails['graphicsCardModel'] != "") { str += "Graphics Card: " + offerDetails['graphicsCardModel'] + "<br/>"; }
        if (offerDetails['ramSize'] != "") { str += "RAM size: " + offerDetails['ramSize'] + " GB<br/>"; }
        if (offerDetails['diskType'] != "") { str += "Disk type: " + offerDetails['diskType'] + "<br/>"; }
        if (offerDetails['matrixResolution'] != "") { str += "Matrix resolution: " + offerDetails['matrixResolution'] + " <br/><br/>"; }
        if (offerDetails['Price'] != "") { str += "Price: " + offerDetails['Price'] + " zł"; }

        return str;
    }

    loadOffers() {
        preloader.addPreloader();
        var classInstance = this;
        $.ajax({
            url: offersServiceUrl,
            method: "POST",
            data: {userHash: cookie.get('user_hash')},
            success: function (response) {
                var maxPrice = -1;
                var minPrice = -1;

                $.each(response, function (key, value) {
                    value["item1"]["rating"] = value["item2"];
                    value["item1"]["Price"] = parseFloat(value["item1"]["Price"]);
                    classInstance.offers.push(value["item1"]);

                    if (value["item1"]['Price'] > maxPrice) {
                        maxPrice = value["item1"]['Price'];
                    }

                    if (value["item1"]['Price'] < minPrice || minPrice == -1) {
                        minPrice = value["item1"]['Price'];
                    }
                })

                $('.priceSlider').rangeSlider("option", "bounds", { min: minPrice, max: maxPrice });
                $('.priceSlider').rangeSlider("min", minPrice);
                $('.priceSlider').rangeSlider("max", maxPrice);

                classInstance.refreshOffers();
                var paginationManagerInstance = new paginationManager("offerCard", 5, "paginationContainer");
            }
        });
    }

    refreshOffers() {
        $('.offerCard').remove();

        var classInstance = this;
        $.each(classInstance.offers, function (key, value) {
            $('.offersContainer').append(classInstance.prepareOfferCard(value));
        })

        preloader.removePreloader();
    }

    updateFilters() {
        preloader.addPreloader();
        var classInstance = this;
        var filters = {};
        filters['minPrice'] = Math.ceil($('.priceSlider').rangeSlider("min"));
        filters['maxPrice'] = Math.ceil($('.priceSlider').rangeSlider("max"));
        filters['processorSeries'] = $('.processorSeries select').val();
        filters['diskType'] = $('.diskType select').val();
        filters['minRamSize'] = parseFloat($('.minRamSize input').val()) || "";
        filters['minDiskSize'] = parseFloat($('.minDiskSize input').val()) || "";
        filters['matrixResolution'] = $('.matrixResolution select').val();

        $.ajax({
            url: offersServiceUrl,
            method: "POST",
            data: { userHash: cookie.get('user_hash'), filters: filters },
            success: function (response) {
                classInstance.offers = [];
                $.each(response, function (key, value) {
                    value["item1"]["rating"] = value["item2"];
                    classInstance.offers.push(value["item1"]);
                })

                classInstance.refreshOffers();
                var paginationManagerInstance = new paginationManager("offerCard", 5, "paginationContainer");
            }
        });

        $('.buttonContainer button.confirm').remove();
    }
}

$(document).ready(function () {
    var offerManagerInstance = new offersManager();
    offerManagerInstance.loadOffers();

    $('select, input').on('change', function () {
        if ($('.buttonContainer button.confirm').length == 0) {
            $('.buttonContainer').append('<button type="button" class="btn btn-primary confirm">Confirm</button>');
        }
    })

    $(".priceSlider").bind("valuesChanging", function (e, data) {
        if ($('.buttonContainer button.confirm').length == 0) {
            $('.buttonContainer').append('<button type="button" class="btn btn-primary confirm">Confirm</button>');
        }
    });

    $('.priceSlider').rangeSlider({ arrows: false });

    $('body').on('click', 'button.confirm', function () {
        offerManagerInstance.updateFilters();
    });
})