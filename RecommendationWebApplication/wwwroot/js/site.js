﻿var offersServiceUrl = "http://localhost:62663/api/Offers";
var statisticsServiceUrl = "http://localhost:62663/api/UserStatistics";
var usersServiceUrl = "http://localhost:62663/api/Users";
var recommendationsServiceUrl = "http://localhost:62663/api/Recommendations";

class Preloader {
    addPreloader() {
        if ($('#preloader').length == 0) {
            $('body').append('<div id = "preloader"><div id="loader"></div></div>');
        }
    }

    removePreloader() {
        $('#preloader').remove();
    }
}

function checkUserHash() {
    if (cookie.get('user_hash') == undefined || cookie.get('user_hash').length <= 0) {
        if (cookie.get('user_profile') != undefined) {
            $.ajax({
                url: usersServiceUrl + "/create",
                method: "POST",
                async: false,
                data: { userProfile: cookie.get('user_profile') },
                success: function (response) {
                    cookie.set('user_hash', response);
                }
            })
        } else {
            if (window.location.href.split("/")[window.location.href.split("/").length - 1] != "") {
                window.location.href = "/";
            }
        }
    }
}

var preloader = new Preloader();

$(document).ready(function () {
    checkUserHash();
    var urlArray = window.location.href.split("/");
    var hash = urlArray[urlArray.length - 1];

    $('a[href="/' + hash + '"]').addClass('active');

    $('body').on('click', '.showOfferButton', function () {
        $.ajax({
            url: statisticsServiceUrl + "/add",
            method: "POST",
            data: { userHash: cookie.get('user_hash'), offerUrl: $(this).attr('href') }
        });
    });

    setTimeout(function () {
        $(document).tooltip();
    }, 1);
})