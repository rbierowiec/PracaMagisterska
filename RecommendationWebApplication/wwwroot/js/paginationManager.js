﻿class paginationManager {
    constructor(itemClass, itemsInPage, paginationContainerClass) {
        this.itemsInPage = itemsInPage;
        this.itemClass = itemClass;
        this.paginationContainerClass = paginationContainerClass;
        this.itemCount = $('.' + this.itemClass).length;
        this.pagesNumber = Math.ceil(this.itemCount / this.itemsInPage);

        this.preparePaginationContainer();
        this.registerServices();
    }

    preparePaginationContainer() {
        var str = "<nav aria-label='navigation'>";
                str += "<ul class='pagination'>";
                    str += "<li class='page-item arrow previous disabled'>";
                        str += "<a class='page-link' aria-label='Previous'>";
                            str += "<span aria-hidden='true'>«</span>";
                            str += "<span class='sr-only'>Previous</span>";
                        str += "</a>";
                    str += "</li>";

                    for (var i = 1; i <= this.pagesNumber; i++) {
                        str += "<li class='page-item' page='" + i +"'><a class='page-link'>"+i+"</a></li>";
                    }

                    str += "<li class='page-item arrow next'>";
                        str += "<a class='page-link' aria-label='Next'>";
                            str += "<span aria-hidden='true'>»</span>";
                            str += "<span class='sr-only'>Next</span>";
                        str += "</a>";
                    str += "</li>";
            str += "</ul>";
        str += "</nav>";

        $('.' + this.paginationContainerClass).html(str);
    }

    registerServices() {
        var classInstance = this;
        $('body').on('click', '.' + classInstance.paginationContainerClass + ' .page-item:not(.active):not(.arrow)', function () {
            $('.' + classInstance.paginationContainerClass + ' .page-item.active').removeClass('active');
            $(this).addClass('active');

            $('.' + classInstance.itemClass).hide();

            var startingIndex = ($(this).attr('page') - 1) * classInstance.itemsInPage;
            $('.' + classInstance.itemClass).slice(startingIndex, startingIndex + classInstance.itemsInPage).show();

            if ($(this).attr('page') > 1) {
                $('.' + classInstance.paginationContainerClass + ' .previous').removeClass('disabled');
            } else {
                $('.' + classInstance.paginationContainerClass + ' .previous').addClass('disabled');
            }

            if ($(this).attr('page') < classInstance.pagesNumber) {
                $('.' + classInstance.paginationContainerClass + ' .next').removeClass('disabled');
            } else {
                $('.' + classInstance.paginationContainerClass + ' .next').addClass('disabled');
            }
        })

        $('body').on('click', '.' + classInstance.paginationContainerClass + ' .next:not(.disabled)', function () {
            var currentActivePage = parseInt($('.' + classInstance.paginationContainerClass + ' .page-item.active').attr('page'));

            $('.' + classInstance.paginationContainerClass + ' .page-item[page=' + (currentActivePage + 1) + ']').click();

            $('.' + classInstance.paginationContainerClass + ' .previous').removeClass('disabled');
            if (currentActivePage + 1 == classInstance.pagesNumber) {
                $(this).addClass('disabled');
            }
        });

        $('body').on('click', '.' + classInstance.paginationContainerClass + ' .previous:not(.disabled)', function () {
            var currentActivePage = parseInt($('.' + classInstance.paginationContainerClass + ' .page-item.active').attr('page'));

            $('.' + classInstance.paginationContainerClass + ' .page-item[page=' + (currentActivePage - 1) + ']').click();

            $('.' + classInstance.paginationContainerClass + ' .next').removeClass('disabled');
            if (currentActivePage - 1 == 1) {
                $(this).addClass('disabled');
            }
        });



        $('.' + this.paginationContainerClass + ' .page-item[page=1]').click();
    }
}