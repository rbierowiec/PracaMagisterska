﻿class recommendationManager {
    prepareOfferCard(offerDetails) {
        var str = "";

        str += '<div class="col col-md-4">'
            str += '<div class="card offerCard">';
                str += '<div class="card-img-top image">';
                    str += '<img src="' + offerDetails['imageUrl'] + '">';
                str += '</div>';
                str += '<div class="card-body">';
                    str += '<h4 class="card-title">' + offerDetails['name'].substr(0, 30) + '</h4>';
                    str += '<p class="card-text">' + this.prepareOfferDescription(offerDetails) + '</p>';
                    str += '<a href="' + offerDetails['url'] + '" class="btn btn-primary showOfferButton">Show offer</a>';
                str += '</div>';
            str += '</div>';
        str += '</div>';

        return str;
    }

    prepareOfferDescription(offerDetails) {
        var str = "";

        if (offerDetails['processorModel'] != null) { str += "Processor: " + offerDetails['processorModel'] + "<br/>"; }
        if (offerDetails['graphicsCardModel'] != null) { str += "Graphics Card: " + offerDetails['graphicsCardModel'] + "<br/>"; }
        if (offerDetails['ramSize'] != null) { str += "RAM size: " + offerDetails['ramSize'] + " GB<br/>"; }
        if (offerDetails['diskType'] != null) { str += "Disk type: " + offerDetails['diskType'] + "<br/>"; }
        if (offerDetails['matrixResolution'] != null) { str += "Matrix resolution: " + offerDetails['matrixResolution'] + " <br/><br/>"; }
        if (offerDetails['price'] != null) { str += "Price: " + offerDetails['price'] + " zł"; }

        return str;
    }

    loadOffers() {
        preloader.addPreloader();
        var classInstance = this;
        $.ajax({
            url: recommendationsServiceUrl + "/similarUsers",
            method: "GET",
            data: { userHash: cookie.get('user_hash') },
            success: function (response) {

                var itemCounter = 0;
                $.each(response, function (key, value) {
                    ++itemCounter;

                    if (itemCounter == 1) {
                        $('.usersRecommendation .carousel-inner').append('<div class="row row-equal carousel-item m-t-0"></div>');
                    }

                    $('.usersRecommendation .carousel-inner .row-equal:last').append(classInstance.prepareOfferCard(value));

                    if (itemCounter == 3) {
                        itemCounter = 0;
                    }
                });

                $('.usersRecommendation .carousel-inner .row-equal:first').addClass('active');
                preloader.removePreloader();
            }
        });

        $.ajax({
            url: recommendationsServiceUrl + "/profiledOffers",
            method: "GET",
            data: { userHash: cookie.get('user_hash') },
            success: function (response) {

                var itemCounter = 0;
                $.each(response, function (key, value) {
                    ++itemCounter;

                    if (itemCounter == 1) {
                        $('.profileRecommendation .carousel-inner').append('<div class="row row-equal carousel-item m-t-0"></div>');
                    }

                    $('.profileRecommendation .carousel-inner .row-equal:last').append(classInstance.prepareOfferCard(value));

                    if (itemCounter == 3) {
                        itemCounter = 0;
                    }
                });

                $('.profileRecommendation .carousel-inner .row-equal:first').addClass('active');
                preloader.removePreloader();
            }
        });
    }
}

$(document).ready(function () {
    var recommendationManagerInstance = new recommendationManager();
    recommendationManagerInstance.loadOffers();
    // manual carousel controls
    $('.next').click(function () { $('.carousel[data-ride=' + $(this).closest('section').attr('data-ride') + ']').carousel('next'); return false; });
    $('.prev').click(function () { $('.carousel[data-ride=' + $(this).closest('section').attr('data-ride') + ']').carousel('prev'); return false; });
})