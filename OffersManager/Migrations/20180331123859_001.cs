﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OffersManager.Migrations
{
    public partial class _001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Offers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    diskSize = table.Column<float>(nullable: false),
                    diskType = table.Column<string>(nullable: true),
                    graphicsCardMemory = table.Column<float>(nullable: false),
                    graphicsCardModel = table.Column<string>(nullable: true),
                    graphicsCardType = table.Column<string>(nullable: true),
                    matrixCoating = table.Column<string>(nullable: true),
                    matrixResolution = table.Column<string>(nullable: true),
                    matrixSize = table.Column<string>(nullable: true),
                    matrixType = table.Column<string>(nullable: true),
                    operatingSystem = table.Column<string>(nullable: true),
                    processorModel = table.Column<string>(nullable: true),
                    processorSeries = table.Column<string>(nullable: true),
                    ramSize = table.Column<float>(nullable: false),
                    ramType = table.Column<string>(nullable: true),
                    weight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Offers");
        }
    }
}
