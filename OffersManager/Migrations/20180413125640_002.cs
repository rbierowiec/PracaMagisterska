﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OffersManager.Migrations
{
    public partial class _002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "authorId",
                table: "Offers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "sourceId",
                table: "Offers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "OfferAuthors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    externalId = table.Column<string>(nullable: true),
                    notRecommendCount = table.Column<int>(nullable: false),
                    rating = table.Column<decimal>(nullable: false),
                    recommendCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferAuthors", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferAuthors");

            migrationBuilder.DropColumn(
                name: "authorId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "sourceId",
                table: "Offers");
        }
    }
}
