﻿using Library;
using OffersManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OffersManager.Classes.RecommendationEngine {
    public class RecommendationEngine
    {
        private readonly NotebookCharacteristicsManager notebookCharacteristicsManager;

        private IDictionary<string, double> characteristicsWeight;

        public RecommendationEngine(NotebookCharacteristicsManager _notebookCharacteristicsManager) {
            this.notebookCharacteristicsManager = _notebookCharacteristicsManager;

            this.characteristicsWeight = new Dictionary<string, double>();

            this.characteristicsWeight["diskRateWeight"] = 1;
            this.characteristicsWeight["graphicsCardRateWeight"] = 1;
            this.characteristicsWeight["matrixRateWeight"] = 1;
            this.characteristicsWeight["processorRateWeight"] = 1;
            this.characteristicsWeight["priceRateWeight"] = 1;
        }

        private void setUserProfile(string userProfile) {
            if(userProfile != null) {
                if (userProfile.Equals("gaming")) {
                    this.characteristicsWeight["priceRateWeight"] = 0.1;
                    this.characteristicsWeight["matrixRateWeight"] = 0.5;
                    this.characteristicsWeight["diskRateWeight"] = 0.5;
                    this.characteristicsWeight["processorRateWeight"] = 0.7;
                } else if (userProfile.Equals("office")) {
                    this.characteristicsWeight["priceRateWeight"] = 0.8;
                    this.characteristicsWeight["matrixRateWeight"] = 0.5;
                    this.characteristicsWeight["graphicsCardRateWeight"] = 0;
                } else if (userProfile.Equals("student")) {
                    this.characteristicsWeight["processorRateWeight"] = 0.5;
                    this.characteristicsWeight["graphicsCardRateWeight"] = 0.3;
                    this.characteristicsWeight["matrixRateWeight"] = 0.1;
                    this.characteristicsWeight["diskRateWeight"] = 0.1;
                }
            }
        }

        private int RateOfferUsingCharacteristics(NotebookCharacteristics characteristics) {
            int rate = 0;

            double tmpValue = 0;

            tmpValue += Math.Pow(characteristics.diskRate * this.characteristicsWeight["priceRateWeight"], 2);
            tmpValue += Math.Pow(characteristics.graphicsCardRate * this.characteristicsWeight["graphicsCardRateWeight"], 2);
            tmpValue += Math.Pow(characteristics.matrixRate * this.characteristicsWeight["matrixRateWeight"], 2);
            tmpValue += Math.Pow(characteristics.priceRate * this.characteristicsWeight["priceRateWeight"], 2);
            tmpValue += Math.Pow(characteristics.processorRate * this.characteristicsWeight["processorRateWeight"], 2);

            rate = (int)Math.Sqrt(tmpValue);

            return rate;
        }

        public List<(Offer, NotebookCharacteristics)> PrepareOfferCharacteristics(List<Offer> offers) {
            List<(Offer, NotebookCharacteristics)> result = new List<(Offer, NotebookCharacteristics)>();

            if (offers.Count() > 1) {
                decimal maxPrice = offers.Aggregate((l, r) => l.Price > r.Price ? l : r).Price;
                decimal minPrice = offers.Aggregate((l, r) => l.Price < r.Price && l.Price != 1 ? l : r).Price;

                this.notebookCharacteristicsManager.SetMinMaxSampleValue(minPrice, maxPrice);

                foreach (var offer in offers) {
                    var offerCharacteristics = this.notebookCharacteristicsManager.PrepareNotebookCharacteristics(offer);

                    result.Add((offer, offerCharacteristics));
                }
            }

            return result;
        }

        public List<(Offer, int)> PrepareOffersRating(List<Offer> offers, string userProfile) {
            if(userProfile != "") {
                this.setUserProfile(userProfile);
            }
            List<(Offer, int)> result = new List<(Offer, int)>();

            if(offers.Count() > 1) {
                decimal maxPrice = offers.Aggregate((l, r) => l.Price > r.Price ? l : r).Price;
                decimal minPrice = offers.Aggregate((l, r) => l.Price < r.Price && l.Price != 1 ? l : r).Price;

                this.notebookCharacteristicsManager.SetMinMaxSampleValue(minPrice, maxPrice);
            }

            foreach (var offer in offers) {
                var offerCharacteristics = this.notebookCharacteristicsManager.PrepareNotebookCharacteristics(offer);

                result.Add((offer, this.RateOfferUsingCharacteristics(offerCharacteristics)));
            }

            result.Sort((item1, item2) => item1.Item2.CompareTo(item2.Item2)) ;
            result.Reverse();

            return result;
        }
    }
}
