﻿using Library;
using OffersManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.Classes.RecommendationEngine {
    public class NotebookCharacteristicsManager {
        private readonly IProcessorsRepository dbProcessors;
        private readonly IGraphicsCardsRepository dbGraphicsCards;

        private decimal minPriceValue = 0;
        private decimal maxPriceValue = 0;

        public NotebookCharacteristicsManager(IProcessorsRepository _dbProcessors, IGraphicsCardsRepository _dbGraphicsCards) {
            this.dbProcessors = _dbProcessors;
            this.dbGraphicsCards = _dbGraphicsCards;
        }

        private int RateNotebookMatrix(Offer offer) {
            int rate = 0;

            switch (offer.matrixCoating) {
                case "matowa":
                case "antyrefleksyjna":
                    rate += 50;
                    break;
                case "błyszcząca antyrefleksyjna":
                    rate += 25;
                    break;
                default:
                    rate += 15;
                    break;
            }

            switch (offer.matrixType) {
                case "IPS":
                    rate += 50;
                    break;
                case "TN":
                default:
                    rate += 25;
                    break;
            }

            return rate;
        }

        private int RateNotebookDisk(Offer offer) {
            int rate = 0;

            switch (offer.diskType) {
                case "SSD + HDD":
                    rate += 75;
                    break;
                case "SSD":
                    rate += 50;
                    break;
                default:
                    rate += 25;
                    break;
            }

            if(offer.diskSize >= 1256) {
                rate += 25;
            }else if(offer.diskSize >= 1000) {
                rate += 15;
            } else {
                rate += 5;
            }

            return rate;
        }

        private int rateProcessor(Offer offer) {
            int rate = 0;

            string processorModel = (offer.processorModel != null) ? offer.processorModel : offer.processorSeries;
            if(processorModel != null) {
                List<Processor> processors = dbProcessors.GetAllProcessors();
                Processor findedProcessor = processors.Where(x => x.processorName.IndexOf(processorModel, StringComparison.CurrentCultureIgnoreCase) >= 0).LastOrDefault();
                int processorRank = (findedProcessor != null) ? findedProcessor.rank : 0;

                if(processorRank > 0) {
                    rate = (processors.Count() - processorRank + 1) * 100 / processors.Count();
                }
            }

            return rate;
        }

        private int RateGraphicsCard(Offer offer) {
            int rate = 0;

            string graphicsCardModel = offer.graphicsCardModel;

            if (graphicsCardModel != null) {
                List<GraphicsCard> graphicsCards = dbGraphicsCards.GetAllGraphicsCards();
                GraphicsCard findedGraphicsCard = graphicsCards.Where(x => x.graphicsCardName.IndexOf(graphicsCardModel, StringComparison.CurrentCultureIgnoreCase) >= 0).LastOrDefault();
                int graphicsCardRank = (findedGraphicsCard != null) ? findedGraphicsCard.rank : 0;

                if (graphicsCardRank > 0) {
                    rate = (graphicsCards.Count() - graphicsCardRank + 1) * 100 / graphicsCards.Count();
                }
            } else {
                if(offer.graphicsCardType != null && offer.graphicsCardType.IndexOf("grafika dedykowana") >= 0) {
                    rate += 20;
                }
            }

            return rate;
        }

        private int RateOfferPrice(Offer offer) {
            int rate = 0;

            if(offer.Price > this.minPriceValue) {
                rate = 100 - (int)(((offer.Price - minPriceValue) / (maxPriceValue - minPriceValue)) * 100);
            } else {
                rate = 100;
            }

            return rate;
        }

        public void SetMinMaxSampleValue(decimal minValue, decimal maxValue) {
            this.minPriceValue = minValue;
            this.maxPriceValue = maxValue;
        }

        public NotebookCharacteristics PrepareNotebookCharacteristics(Offer offer) {
            NotebookCharacteristics characteristics = new NotebookCharacteristics();

            characteristics.matrixRate = this.RateNotebookMatrix(offer);
            characteristics.diskRate = this.RateNotebookDisk(offer);
            characteristics.processorRate = this.rateProcessor(offer);
            characteristics.graphicsCardRate = this.RateGraphicsCard(offer);
            characteristics.priceRate = this.RateOfferPrice(offer);

            return characteristics;
        }
    }
}
