﻿using Library;
using System.Collections.Generic;

namespace OffersManager.Interfaces {
    public interface IUsersRepository {
        int AddUser(User user);
        int CheckIfUserExists(string hash);
        User GetUser(int id);
        List<User> GetAllUsers();
        bool UpdateUser(User user);
        bool DeleteUser(int id);
    }
}
