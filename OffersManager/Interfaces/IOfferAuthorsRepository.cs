﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OffersManager.Interfaces
{
    public interface IOfferAuthorsRepository {
        int AddAuthor(OfferAuthor author);
        bool checkIfAuthorChanged(OfferAuthor author);
        OfferAuthor GetAuthor(int id);
        List<OfferAuthor> GetAllAuthors();
        bool UpdateAuthor(OfferAuthor author);
        bool DeleteAuthor(int id);
    }
}
