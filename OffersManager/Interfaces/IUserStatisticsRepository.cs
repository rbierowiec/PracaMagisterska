﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OffersManager.Interfaces
{
    public interface IUserStatisticsRepository {
        int AddUserStatistic(UserStatistic statistic);
        Offer GetOfferFromStatistic(int id);
        List<UserStatistic> GetAllStatistics();
        List<UserStatistic> GetAllUserStatistics(string userHash);
    }
}
