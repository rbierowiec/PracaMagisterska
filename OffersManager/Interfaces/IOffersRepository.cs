﻿using Library;
using System.Collections.Generic;

namespace OffersManager.Interfaces {
    public interface IOffersRepository {
        int AddOffer(Offer offer);
        bool checkIfOfferExists(string url);
        Offer GetOffer(int id);
        List<Offer> GetAllOffers();
        List<Offer> GetFilteredOffers(OfferFilters filters);
        bool UpdateOffer(Offer offer);
        bool DeleteOffer(int id);
    }
}
