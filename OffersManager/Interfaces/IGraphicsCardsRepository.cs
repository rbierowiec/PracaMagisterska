﻿using Library;
using System.Collections.Generic;

namespace OffersManager.Interfaces {
    public interface IGraphicsCardsRepository{
        int AddGraphicsCard(GraphicsCard card);
        List<GraphicsCard> GetAllGraphicsCards();
        bool RemoveAllGraphicsCards();
    }
}
