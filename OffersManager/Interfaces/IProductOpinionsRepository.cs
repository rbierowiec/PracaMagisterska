﻿using Library;
using System.Collections.Generic;

namespace OffersManager.Interfaces {
    public interface IProductOpinionsRepository {
        int AddProductOpinion(ProductOpinion ProductOpinion);
        int checkIfProductOpinionExists(string url);
        ProductOpinion GetOpinion(int id);
        List<ProductOpinion> GetAllProductOpinions();
        bool UpdateProductOpinion(ProductOpinion opinion);
        bool DeleteProductOpinion(int id);
    }
}
