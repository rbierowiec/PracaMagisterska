﻿using Library;
using System.Collections.Generic;

namespace OffersManager.Interfaces {
    public interface IProcessorsRepository {
        int AddProcessor(Processor proc);
        List<Processor> GetAllProcessors();
        bool RemoveAllProcessors();
    }
}
