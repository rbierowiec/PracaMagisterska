﻿using Library;
using Microsoft.EntityFrameworkCore;

namespace OffersManager.Contexts {
    public class MyContext : DbContext {
        public MyContext(DbContextOptions<MyContext> options)
          : base(options) { }

        public DbSet<Offer> Offers { get; set; }
        public DbSet<OfferAuthor> OfferAuthors { get; set; }
        public DbSet<ProductOpinion> ProductOpinions { get; set; }
        public DbSet<GraphicsCard> GraphicsCards { get; set; }
        public DbSet<Processor> Processors { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserStatistic> UserStatistics { get; set; }

    }
}
