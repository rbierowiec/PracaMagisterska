﻿using Library;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class UsersRepository : IUsersRepository {
        private MyContext db;

        public UsersRepository(MyContext _db) {
            db = _db;
        }

        public int AddUser(User user) {
            db.Users.Add(user);
            db.SaveChanges();

            return user.Id;
        }

        public int CheckIfUserExists(string hash) {
            if (db.Users.Where(x => x.Hash.Equals(hash)).Count() > 0) {
                return db.Users.Where(x => x.Hash.Equals(hash)).First().Id;
            } else {
                return -1;
            }
        }

        public User GetUser(int id) {
            return db.Users.Find(id);
        }

        public List<User> GetAllUsers() {
            return db.Users.ToList();
        }

        public bool UpdateUser(User user) {
            var itemToUpdate = db.Users.Find(user.Id);
            db.Entry(itemToUpdate).CurrentValues.SetValues(user);

            return db.SaveChanges() > 0;
        }

        public bool DeleteUser(int id) {
            try {
                db.Users.Remove(db.Users.Find(id));
                db.SaveChanges();
            } catch {
                return false;
            }

            return true;
        }
    }
}
