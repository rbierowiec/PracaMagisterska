﻿using Library;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class UserStatisticsRepository : IUserStatisticsRepository {
        private MyContext db;

        public UserStatisticsRepository(MyContext _db) {
            db = _db;
        }

        public int AddUserStatistic(UserStatistic statistic) {
            db.UserStatistics.Add(statistic);
            db.SaveChanges();

            return statistic.Id;
        }

        public Offer GetOfferFromStatistic(int id) {
            return db.Offers.Where(x => x.Url.Equals(db.UserStatistics.Find(id).OfferUrl)).First();
        }

        public List<UserStatistic> GetAllStatistics() {
            return db.UserStatistics.ToList();
        }

        public List<UserStatistic> GetAllUserStatistics(string userHash) {
            User user = db.Users.Where(u => u.Hash == userHash).FirstOrDefault();

            if(user != null) {
                return db.UserStatistics.Where(x => x.UserId == user.Id).ToList();
            } else {
                return new List<UserStatistic>();
            }
        }
        
    }
}
