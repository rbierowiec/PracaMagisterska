﻿using Library;
using Microsoft.EntityFrameworkCore;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class OfferAuthorsRepository : IOfferAuthorsRepository {
        private MyContext db;

        public OfferAuthorsRepository(MyContext _db) {
            db = _db;
        }

        public int AddAuthor(OfferAuthor author) {
            db.OfferAuthors.Add(author);
            db.SaveChanges();

            return author.Id;
        }

        public OfferAuthor GetAuthor(int id) {
            return db.OfferAuthors.Find(id);
        }

        public bool checkIfAuthorChanged(OfferAuthor author) {
            return db.OfferAuthors.Where(x => x.Name == author.Name && x.recommendCount == author.recommendCount && x.notRecommendCount == author.notRecommendCount).Count() == 0;
        }

        public List<OfferAuthor> GetAllAuthors() {
            return parseToList(db.OfferAuthors);
        }

        public bool UpdateAuthor(OfferAuthor author) {
            var itemToUpdate = db.OfferAuthors.Find(author.Id);
            db.Entry(itemToUpdate).CurrentValues.SetValues(author);

            return db.SaveChanges() > 0;
        }

        public bool DeleteAuthor(int id) {
            try {
                db.OfferAuthors.Remove(db.OfferAuthors.Find(id));
                db.SaveChanges();
            } catch {
                return false;
            }

            return true;
        }

        private static List<OfferAuthor> parseToList(DbSet<OfferAuthor> dbset) {
            List<OfferAuthor> authors = new List<OfferAuthor>();

            foreach (var offer in dbset) {
                authors.Add(offer);
            }

            return authors;
        }
    }
}
