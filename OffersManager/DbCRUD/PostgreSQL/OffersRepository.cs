﻿using Library;
using Microsoft.EntityFrameworkCore;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class OffersRepository : IOffersRepository {
        private MyContext db;

        public OffersRepository(MyContext _db) {
            db = _db;
        }

        public int AddOffer(Offer offer) {
            db.Offers.Add(offer);
            db.SaveChanges();

            return offer.Id;
        }

        public Offer GetOffer(int id) {
            return db.Offers.Find(id);
        }

        public bool checkIfOfferExists(string url) {
            return db.Offers.Where(x => x.Url == url).Count() > 0;
        }

        public List<Offer> GetAllOffers() {
            return parseToList(db.Offers);
        }

        public List<Offer> GetFilteredOffers(OfferFilters filters) {
            var filteredOffers = this.GetAllOffers();

            if(filters.maxPrice > 0) {
                filteredOffers = filteredOffers.Where(x => x.Price <= filters.maxPrice).ToList();
            }

            if(filters.minPrice > 0) {
                filteredOffers = filteredOffers.Where(x => x.Price >= filters.minPrice).ToList();
            }

            if (filters.processorSeries.Length > 0 && filters.processorSeries != "*") {
                if (filters.processorSeries == "other") {
                    filteredOffers = filteredOffers.Where(x => !x.processorSeries.Contains("Intel Core i3") && !x.processorSeries.Contains("Intel Core i5") && !x.processorSeries.Contains("Intel Core i7")).ToList();
                } else {
                    filteredOffers = filteredOffers.Where(x => x.processorSeries.Contains(filters.processorSeries)).ToList();
                }
            }

            if (filters.diskType.Length > 0 && filters.diskType != "*") {
                if (filters.diskType == "other") {
                    filteredOffers = filteredOffers.Where(x => !x.diskType.Contains("HDD") && !x.diskType.Contains("SSD")).ToList();
                } else { 
                    filteredOffers = filteredOffers.Where(x => x.diskType.Contains(filters.diskType)).ToList();
                }
            }

            if (filters.minDiskSize > 0) {
                filteredOffers = filteredOffers.Where(x => x.diskSize >= filters.minDiskSize).ToList();
            }

            if (filters.minRamSize > 0) {
                filteredOffers = filteredOffers.Where(x => x.ramSize >= filters.minRamSize).ToList();
            }
            if (filters.matrixResolution.Length > 0 && filters.matrixResolution != "*") {
                if (filters.matrixResolution == "other") {
                    filteredOffers = filteredOffers.Where(x => !x.matrixResolution.Contains("1366 x 768") && !x.matrixResolution.Contains("1600 x 900") && !x.matrixResolution.Contains("1920 x 1080") && !x.matrixResolution.Contains("3840 x 2160")).ToList();
                } else {
                    filteredOffers = filteredOffers.Where(x => x.matrixResolution.Contains(filters.matrixResolution)).ToList();
                }
            }

            return filteredOffers;
        }

        public bool UpdateOffer(Offer offer) {
            var itemToUpdate = db.Offers.Find(offer.Id);
            db.Entry(itemToUpdate).CurrentValues.SetValues(offer);

            return db.SaveChanges() > 0;
        }

        public bool DeleteOffer(int id) {
            try {
                db.Offers.Remove(db.Offers.Find(id));
                db.SaveChanges();
            } catch {
                return false;
            }

            return true;
        }
        
        private static List<Offer> parseToList(DbSet<Offer> dbset) {
            List<Offer> offers = new List<Offer>();

            foreach (var offer in dbset) {
                offers.Add(offer);
            }

            return offers;
        }
    }
}
