﻿using Library;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class ProcessorsRepository : IProcessorsRepository {
        private MyContext db;

        public ProcessorsRepository(MyContext _db) {
            db = _db;
        }

        public int AddProcessor(Processor proc) {
            db.Processors.Add(proc);
            db.SaveChanges();

            return proc.id;
        }

        public List<Processor> GetAllProcessors() {
            return db.Processors.ToList();
        }

        public bool RemoveAllProcessors() {
            db.Processors.RemoveRange(db.Processors);
            db.SaveChanges();

            return db.GraphicsCards.Count() == 0;
        }
    }
}
