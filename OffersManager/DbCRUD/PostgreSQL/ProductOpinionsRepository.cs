﻿using Library;
using Microsoft.EntityFrameworkCore;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class ProductOpinionsRepository : IProductOpinionsRepository {
        private MyContext db;

        public ProductOpinionsRepository(MyContext _db) {
            db = _db;
        }

        public int AddProductOpinion(ProductOpinion opinion) {
            db.ProductOpinions.Add(opinion);
            db.SaveChanges();

            return opinion.id;
        }

        public int checkIfProductOpinionExists(string url) {
            if(db.ProductOpinions.Where(x => x.opinionSource == url).Count() > 0) {
                return db.ProductOpinions.Where(x => x.opinionSource == url).First().id;
            } else {
                return -1;
            }
        }

        public ProductOpinion GetOpinion(int id) {
            return db.ProductOpinions.Find(id);
        }

        public List<ProductOpinion> GetAllProductOpinions() {
            return parseToList(db.ProductOpinions);
        }

        public bool UpdateProductOpinion(ProductOpinion opinion) {
            var itemToUpdate = db.ProductOpinions.Find(opinion.id);
            db.Entry(itemToUpdate).CurrentValues.SetValues(opinion);

            return db.SaveChanges() > 0;
        }

        public bool DeleteProductOpinion(int id) {
            try {
                db.ProductOpinions.Remove(db.ProductOpinions.Find(id));
                db.SaveChanges();
            } catch {
                return false;
            }

            return true;
        }

        private static List<ProductOpinion> parseToList(DbSet<ProductOpinion> dbset) {
            List<ProductOpinion> opinions = new List<ProductOpinion>();

            foreach (var opinion in dbset) {
                opinions.Add(opinion);
            }

            return opinions;
        }
    }
}
