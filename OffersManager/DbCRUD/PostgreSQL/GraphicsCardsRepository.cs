﻿using Library;
using OffersManager.Contexts;
using OffersManager.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OffersManager.DbCRUD.PostgreSQL {
    public class GraphicsCardsRepository : IGraphicsCardsRepository {
        private MyContext db;

        public GraphicsCardsRepository(MyContext _db) {
            db = _db;
        }

        public int AddGraphicsCard(GraphicsCard card) {
            db.GraphicsCards.Add(card);
            db.SaveChanges();

            return card.id;
        }

        public List<GraphicsCard> GetAllGraphicsCards() {
            return db.GraphicsCards.ToList();
        }

        public bool RemoveAllGraphicsCards() {
            db.GraphicsCards.RemoveRange(db.GraphicsCards);
            db.SaveChanges();

            return db.GraphicsCards.Count() == 0;
        }
    }
}
