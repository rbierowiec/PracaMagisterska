﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OffersManager.Interfaces;

namespace OffersManager.Controllers {
    [Produces("application/json")]
    [Route("api/UserStatistics")]
    public class UserStatisticsController : Controller {

        private IHttpContextAccessor accessor;

        private readonly IUsersRepository dbUsers;
        private readonly IUserStatisticsRepository dbStatistics;


        public UserStatisticsController(IUsersRepository _dbUsers, IUserStatisticsRepository _dbStatistics, IHttpContextAccessor _accessor) {
            this.dbUsers = _dbUsers;
            this.dbStatistics = _dbStatistics;

            this.accessor = _accessor;
        }

        [Route("add")]
        [HttpPost]
        public IActionResult AddUserStatistics(string userHash, string offerUrl) {
            UserStatistic statistic = new UserStatistic();
            int userId = dbUsers.CheckIfUserExists(userHash);
            if (userId < 0) {
                userId = dbUsers.AddUser(new User { Hash = userHash, CreationDate = DateTime.Now, Ip = this.accessor.HttpContext.Connection.RemoteIpAddress.ToString() });
            }

            statistic.OfferUrl = offerUrl;
            statistic.UserId = userId;
            statistic.Browser = Request.Headers["User-Agent"].ToString();
            statistic.VisitDate = DateTime.Now;

            dbStatistics.AddUserStatistic(statistic);

            return Ok();
        }
    }
}