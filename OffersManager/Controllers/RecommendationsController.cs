﻿using Library;
using Microsoft.AspNetCore.Mvc;
using OffersManager.Classes.RecommendationEngine;
using OffersManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OffersManager.Controllers {

    [Produces("application/json")]
    [Route("api/Recommendations")]
    public class RecommendationsController : Controller {
        private readonly IOffersRepository dbOffers;
        private readonly IUsersRepository dbUsers;
        private readonly IUserStatisticsRepository dbStatistics;

        private readonly RecommendationEngine recommendationEngine;

        public RecommendationsController(IOffersRepository _dbOffers, IUsersRepository _dbUsers, IUserStatisticsRepository _dbStatistics, RecommendationEngine _recommendationEngine) {
            this.dbOffers = _dbOffers;
            this.dbUsers = _dbUsers;
            this.dbStatistics = _dbStatistics;

            this.recommendationEngine = _recommendationEngine;
        }

        [Route("similarUsers")]
        [HttpGet]
        public IActionResult GetSimilarUsersOffers(string userHash) {
            int UserId = dbUsers.GetAllUsers().Where(x => x.Hash.Equals(userHash)).First().Id;
            List<Offer> similarUsersOffers = new List<Offer>();

            // Pobranie odwiedzanych przez użytkownika linków
            List<UserStatistic> statistics = this.dbStatistics.GetAllUserStatistics(userHash);
            List<string> offerUrls = new List<string>();

            foreach (var item in statistics) {
                offerUrls.Add(item.OfferUrl);
            }

            List<string> similarUsers = new List<string>();
            // Sprawdzenie ile wspólnych ofert odwiedzili inni użytkownicy
            foreach (var otherUserStatistics in this.dbStatistics.GetAllStatistics().Where(x => offerUrls.Contains(x.OfferUrl)).GroupBy(s => s.UserId).Select(u => new { UserId = u.Key, count = u.Count() })) {
                if (otherUserStatistics.count >= offerUrls.Count() * 0.25 && UserId != otherUserStatistics.UserId) {
                    var findedUser = dbUsers.GetAllUsers().Where(x => x.Id == otherUserStatistics.UserId).FirstOrDefault();

                    if (findedUser != null) {
                        similarUsers.Add(findedUser.Hash);
                    }
                }
            }

            foreach (var similarUserHash in similarUsers) {
                foreach (var similarUserUrl in dbStatistics.GetAllUserStatistics(similarUserHash)) {
                    if (similarUsersOffers.Where(x => x.Url.Equals(similarUserUrl.OfferUrl)).Count() == 0 && !offerUrls.Contains(similarUserUrl.OfferUrl)) {
                        similarUsersOffers.Add(dbOffers.GetAllOffers().Where(x => x.Url.Equals(similarUserUrl.OfferUrl)).First());
                    }
                }
            }

            return Ok(similarUsersOffers);
        }


        [Route("profiledOffers")]
        [HttpGet]
        public IActionResult GetProfiledOffers(string userHash) {
            List<Offer> profiledOffers = new List<Offer>();

            // Pobranie odwiedzanych przez użytkownika linków
            List<UserStatistic> statistics = this.dbStatistics.GetAllUserStatistics(userHash);
            List<string> offerUrls = new List<string>();

            foreach (var item in statistics) {
                offerUrls.Add(item.OfferUrl);
            }

            List<(Offer, NotebookCharacteristics)> allOffersCharacteristics = this.recommendationEngine.PrepareOfferCharacteristics(dbOffers.GetAllOffers());

            if (statistics.Count() > 0) {
                NotebookCharacteristics userProfile = new NotebookCharacteristics();

                foreach (var notebookRatings in allOffersCharacteristics) {
                    if (offerUrls.Contains(notebookRatings.Item1.Url)) {
                        userProfile.diskRate += notebookRatings.Item2.diskRate;
                        userProfile.graphicsCardRate += notebookRatings.Item2.graphicsCardRate;
                        userProfile.matrixRate += notebookRatings.Item2.matrixRate;
                        userProfile.priceRate += notebookRatings.Item2.priceRate;
                        userProfile.processorRate += notebookRatings.Item2.processorRate;
                    }
                }

                userProfile.diskRate /= offerUrls.Count();
                userProfile.graphicsCardRate /= offerUrls.Count();
                userProfile.matrixRate /= offerUrls.Count();
                userProfile.priceRate /= offerUrls.Count();
                userProfile.processorRate /= offerUrls.Count();

                foreach (var notebookRatings in allOffersCharacteristics) {
                    var euclideanDistance = Math.Pow((notebookRatings.Item2.diskRate - userProfile.diskRate), 2);
                    euclideanDistance += Math.Pow((notebookRatings.Item2.graphicsCardRate - userProfile.graphicsCardRate), 2);
                    euclideanDistance += Math.Pow((notebookRatings.Item2.matrixRate - userProfile.matrixRate), 2);
                    euclideanDistance += Math.Pow((notebookRatings.Item2.priceRate - userProfile.priceRate), 2);
                    euclideanDistance += Math.Pow((notebookRatings.Item2.processorRate - userProfile.processorRate), 2);

                    euclideanDistance = Math.Sqrt(euclideanDistance / 5);
                    var similarity = (1 / (1 + euclideanDistance)) * 1000;

                    if(similarity > 50) {
                        profiledOffers.Add(notebookRatings.Item1);
                        Console.WriteLine(notebookRatings.Item1.Name + ": " + similarity);
                    }

                }
            }

            return Ok(profiledOffers);
        }
    }
}
