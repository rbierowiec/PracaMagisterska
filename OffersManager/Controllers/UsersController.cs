﻿using Library;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OffersManager.Interfaces;
using System;
using System.Linq;

namespace OffersManager.Controllers {

    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller {

        private IHttpContextAccessor accessor;
        private readonly IUsersRepository dbUsers;

        public UsersController(IUsersRepository _dbUsers, IHttpContextAccessor _accessor) {
            this.dbUsers = _dbUsers;
            this.accessor = _accessor;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult createUser(string userProfile) {
            bool result = true;
            string hash = "";

            while (result) {
                hash = Guid.NewGuid().ToString("N");

                if (dbUsers.GetAllUsers().Where(x => x.Hash.Equals(hash)).Count() == 0) {
                    result = false;
                }
            }

            User user = new User();
            user.Hash = hash;
            user.Profile = userProfile;
            user.Ip = this.accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            user.CreationDate = DateTime.Now;

            dbUsers.AddUser(user);

            return Ok(hash);
        }

    }
}