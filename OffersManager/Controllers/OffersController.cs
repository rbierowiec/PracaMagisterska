﻿using Library;
using Microsoft.AspNetCore.Mvc;
using OffersFromHTMLParser.FeedbackSearchEngine;
using OffersManager.Classes.RecommendationEngine;
using OffersManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OffersManager.Controllers {
    [Produces("application/json")]
    [Route("api/Offers")]
    public class OffersController : Controller {
        private readonly IUsersRepository dbUsers;
        private readonly IOffersRepository dbOffers;
        private readonly IOfferAuthorsRepository dbAuthors;
        private readonly IProductOpinionsRepository dbOpinions;
        private readonly IUserStatisticsRepository dbStatistics;

        private readonly RecommendationEngine recommendationEngine;

        public OffersController(IUsersRepository _dbUsers, IOffersRepository _dbOffers, IOfferAuthorsRepository _dbAuthors, IProductOpinionsRepository _dbOpinions, IUserStatisticsRepository _dbStatistics,RecommendationEngine _recommendationEngine) {
            this.dbUsers = _dbUsers;
            this.dbOffers = _dbOffers;
            this.dbAuthors = _dbAuthors;
            this.dbOpinions = _dbOpinions;
            this.dbStatistics = _dbStatistics;
            this.recommendationEngine = _recommendationEngine;
        }

        [Route("update")]
        [HttpGet]
        public IActionResult GetUserApplications() {
            var returnObject = new List<Object>();

            var offersFromHtmlDownloader = new HtmlContentManager();
            var offerDetailsDownloader = new OfferDetailsDownloader();
            List<String> offers = offersFromHtmlDownloader.downloadAllUrls();

            List<Offer> addedOffers = new List<Offer>();
            List<OfferAuthor> addedAuthors = new List<OfferAuthor>();

            foreach (var offer in offers) {
                var offerDetails = offerDetailsDownloader.downloadOfferDetails(offer);

                int idAuthor = 0;
                if (dbAuthors.checkIfAuthorChanged(offerDetails.Item2)) {
                    addedAuthors.Add(offerDetails.Item2);
                    dbAuthors.AddAuthor(offerDetails.Item2);
                    idAuthor = offerDetails.Item2.Id;
                } else {
                    idAuthor = dbAuthors.GetAllAuthors().Where(x => x.Name == offerDetails.Item2.Name).First().Id;
                }

                if (!dbOffers.checkIfOfferExists(offerDetails.Item1.Url)) {
                    offerDetails.Item1.authorId = idAuthor;
                    addedOffers.Add(offerDetails.Item1);
                    dbOffers.AddOffer(offerDetails.Item1);
                }

            }

            return Ok((addedOffers, addedAuthors));
        }

        [Route("searchForFeedback")]
        [HttpGet]
        public IActionResult getOffersFeedback() {
            List<ProductOpinion> addedOpinions = new List<ProductOpinion>();
            FeedbackSearchEngine feedbackEngine = new FeedbackSearchEngine();
            
            foreach (var offer in dbOffers.GetAllOffers()) {
                if (offer.opinionId == 0) {
                    var opinion = feedbackEngine.getFeedback(offer.Name);
                    int opinionId = dbOpinions.checkIfProductOpinionExists(opinion.opinionSource);

                    if (opinionId < 0) {
                        opinionId = dbOpinions.AddProductOpinion(opinion);
                        addedOpinions.Add(opinion);
                    }

                    offer.opinionId = opinionId;
                }
            }

            return Ok(addedOpinions);
        }


        [Route("")]
        [HttpPost]
        public IActionResult getOffers(string userHash, OfferFilters filters) {
            var offers = new List<Offer>();
            try {
                offers = dbOffers.GetFilteredOffers(filters);
                Console.WriteLine(filters.processorSeries);
            } catch (Exception e){
                Console.WriteLine(e.Message);

            }

            User user = dbUsers.GetAllUsers().Where(x => x.Hash.Equals(userHash)).FirstOrDefault();
            string userProfile = "";
            if(user != null) {
                userProfile = user.Profile;
            }

            List<(Offer, int)> offerRatings = this.recommendationEngine.PrepareOffersRating(offers, userProfile).Take(100).ToList();
            List<(Dictionary<string, string>, int)> offerRatingsWithDetails = new List<(Dictionary<string, string>, int)>();

            foreach (var offer in offerRatings) {
                Dictionary<string, string> tmpObject = new Dictionary<string, string>();

                foreach (var property in offer.Item1.GetType().GetProperties()) {
                    if(property.GetValue(offer.Item1, null) != null) {
                        tmpObject[property.Name] = property.GetValue(offer.Item1, null).ToString();
                    } else {
                        tmpObject[property.Name] = "";
                    }
                }

                tmpObject["authorRatings"] = "";
                tmpObject["laptopRating"] = "";

                var offerAuthor = dbAuthors.GetAuthor(offer.Item1.authorId);
                if(offerAuthor != null && (offerAuthor.recommendCount + offerAuthor.notRecommendCount) > 0) {
                    tmpObject["authorRatings"] = dbAuthors.GetAuthor(offer.Item1.authorId).rating.ToString();
                }
                if(offer.Item1.opinionId > 0) {
                    var productOpinion = dbOpinions.GetOpinion(offer.Item1.opinionId);
                    if(productOpinion != null && productOpinion.numberOfReviews > 0) {
                        tmpObject["laptopRating"] = productOpinion.rating.ToString();
                    }
                }

                offerRatingsWithDetails.Add((tmpObject, offer.Item2));
            }


            return Ok(offerRatingsWithDetails);
        }
        
        [Route("lastVisited")]
        [HttpPost]
        public IActionResult getLastVisited(string userHash) {
            List<UserStatistic> statistics = this.dbStatistics.GetAllUserStatistics(userHash);
            List<string> offerUrls = new List<string>();

            foreach (var item in statistics) {
                offerUrls.Add(item.OfferUrl);
            }

            List<Offer> visitedOffers = dbOffers.GetAllOffers().Where(x => offerUrls.Contains(x.Url)).ToList();

            return Ok(visitedOffers.Take(20));
        }
    }
}