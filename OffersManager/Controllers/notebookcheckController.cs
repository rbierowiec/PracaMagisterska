﻿using Microsoft.AspNetCore.Mvc;
using OffersFromHTMLParser.NotebookPartsRankingsDownloader;
using OffersManager.Interfaces;

namespace OffersManager.Controllers {

    [Produces("application/json")]
    [Route("api/notebookPartsRanking")]
    public class notebookcheckController : Controller {
        private readonly IGraphicsCardsRepository dbGraphicsCards;
        private readonly IProcessorsRepository dbProcessors;

        public notebookcheckController(IGraphicsCardsRepository _dbGraphicsCards, IProcessorsRepository _dbProcessors) {
            this.dbGraphicsCards = _dbGraphicsCards;
            this.dbProcessors = _dbProcessors;
        }

        [Route("graphicsCardUpdate")]
        [HttpGet]
        public IActionResult UpdateGraphicsCard() {
            graphicsCardsRankingDownloader graphicsCardsRankingManager = new graphicsCardsRankingDownloader();

            dbGraphicsCards.RemoveAllGraphicsCards();

            foreach (var card in graphicsCardsRankingManager.downloadGraphicsCardsRanking()) {
                dbGraphicsCards.AddGraphicsCard(card);
            }


            return Ok();
        }

        [Route("processorUpdate")]
        [HttpGet]
        public IActionResult UpdateProcessor() {
            processorsRankingDownloader processorsManager = new processorsRankingDownloader();

            dbProcessors.RemoveAllProcessors();

            foreach (var proc in processorsManager.downloadProcessorsRanking()) {
                dbProcessors.AddProcessor(proc);
            }


            return Ok();
        }
    }
}
