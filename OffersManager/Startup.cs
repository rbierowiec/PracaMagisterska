﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OffersManager.Contexts;
using OffersManager.Interfaces;

namespace OffersManager {
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            
            services.AddMvc();

            var connectionString = Configuration.GetConnectionString("MyContext");
            services.AddEntityFrameworkNpgsql().AddDbContext<MyContext>(options => options.UseNpgsql(connectionString));

            services.AddTransient<IOffersRepository, DbCRUD.PostgreSQL.OffersRepository>();
            services.AddTransient<IOfferAuthorsRepository, DbCRUD.PostgreSQL.OfferAuthorsRepository>();
            services.AddTransient<IProductOpinionsRepository, DbCRUD.PostgreSQL.ProductOpinionsRepository>();
            services.AddTransient<IGraphicsCardsRepository, DbCRUD.PostgreSQL.GraphicsCardsRepository>();
            services.AddTransient<IProcessorsRepository, DbCRUD.PostgreSQL.ProcessorsRepository>();
            services.AddTransient<IUsersRepository, DbCRUD.PostgreSQL.UsersRepository>();
            services.AddTransient<IUserStatisticsRepository, DbCRUD.PostgreSQL.UserStatisticsRepository>();

            services.AddTransient<Classes.RecommendationEngine.NotebookCharacteristicsManager>();
            services.AddTransient<Classes.RecommendationEngine.RecommendationEngine>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.WithOrigins("http://localhost"));

            app.UseMvc();
        }
    }
}
